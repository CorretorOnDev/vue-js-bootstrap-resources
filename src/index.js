import _ from 'lodash'

import MaskDirective from './directives/mask'
import InputNumber from './components/InputNumber.vue'
import InputInteger from './components/InputInteger.vue'
import InputMoney from './components/InputMoney.vue'
import InputDate from './components/InputDate.vue'
import InputDateTime from './components/InputDateTime.vue'
import InputTime from './components/InputTime.vue'
import DataTableSummary from './components/DataTableSummary.vue'
import DataTablePaginator from './components/DataTablePaginator.vue'
import DataTable from './components/DataTable.vue'
import Modal from './components/Modal.vue'
import Select2 from './components/Select2.vue'
import InputMask from './components/InputMask.vue'
import InputPerc from './components/InputPerc.vue'
import InputAutocomplete from './components/InputAutocomplete.vue'
import ScheduleCalendar from './components/ScheduleCalendar.vue'
import ScheduleCalendarMonth from './components/ScheduleCalendarMonth.vue'
import ScheduleCalendarWeek from './components/ScheduleCalendarWeek.vue'
import ScheduleCalendarDay from './components/ScheduleCalendarDay.vue'
import ScheduleCalendarCommitment from './components/ScheduleCalendarCommitment.vue'
import ScheduleCalendarEventPopover from './components/ScheduleCalendarEventPopover.vue'
import RangeSlider from './components/RangeSlider.vue'

export default {
  install (Vue, options) {
    window.$ = window.jQuery = require('jquery')
    // require('bootstrap-sass/assets/stylesheets/_bootstrap.scss')
    require('bootstrap-sass/assets/javascripts/bootstrap')
    require('bootstrap-datepicker')
    require('bootstrap-datepicker/dist/css/bootstrap-datepicker.css')
    require('bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR')
    require('font-awesome/css/font-awesome.css')
    require('meiomask/src/meiomask')
    require('eonasdan-bootstrap-datetimepicker')
    require('eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css')
    window.Hashtable = require('./assets/libs/hashtable')
    require('./assets/jquery/plugins/jquery.numberformatter')
    require('./assets/prototype/String')
    require('./assets/prototype/Number')
    require('./assets/prototype/Image')
    require('select2/dist/css/select2.css')
    require('select2/dist/js/select2.full')
    Vue.directive('mask', MaskDirective)
    Vue.component('input-number', InputNumber)
    Vue.component('input-integer', InputInteger)
    Vue.component('input-money', InputMoney)
    Vue.component('input-date', InputDate)
    Vue.component('input-date-time', InputDateTime)
    Vue.component('input-time', InputTime)
    Vue.component('data-table-summary', DataTableSummary)
    Vue.component('data-table-paginator', DataTablePaginator)
    Vue.component('data-table', DataTable)
    Vue.component('modal', Modal)
    Vue.component('select2', Select2)
    Vue.component('input-mask', InputMask)
    Vue.component('input-perc', InputPerc)
    Vue.component('input-autocomplete', InputAutocomplete)
    Vue.component('schedule-calendar', ScheduleCalendar)
    Vue.component('schedule-calendar-month', ScheduleCalendarMonth)
    Vue.component('schedule-calendar-week', ScheduleCalendarWeek)
    Vue.component('schedule-calendar-day', ScheduleCalendarDay)
    Vue.component('schedule-calendar-commitment', ScheduleCalendarCommitment)
    Vue.component('schedule-calendar-event-popover', ScheduleCalendarEventPopover)

    Vue.component('range-slider', RangeSlider)

    Vue.prototype.$isSelected = function (collection, value) {
      return _.includes(collection.map(value => value.id), value.id)
    }
  }
}
