import $ from 'jquery'
import moment from 'moment'

/**
 * Template Format
 * @param arg0 : String
 * @returns {string}
 */
String.prototype.template = function(arg0) {
  return this.replace(/{([^{}]*)}/g,
    function (a, b) {
      let r = arg0[b];
      return typeof r === 'string' || typeof r === 'number' ? r : a;
    }).replace(/:([^:]*)/g, function (a, b) {
    let r = arg0[b];
    return typeof r === 'string' || typeof r === 'number' ? r : a;
  });
};


/**
 * Repeat string
 * @param times: Number
 * @param val: String
 * @returns {string}
 */
String.prototype.repeat = function(times, val) {
  times = parseInt(times) || 1;
  return (val ? this : '') + Array(++times).join(val ? val : this);
};


/**
 * Remove of the string the characters with accentuation, spaces and specials characters
 * @return {string}
 */
String.prototype.charClean = function() {
  var chars 			= [231, 225, 224, 227, 226, 228, 233, 232, 234, 235, 237, 236, 238, 239, 243, 242, 245, 244, 246, 250, 249, 251, 252, 32, 92, 47];
  var charsReplace	= ['c', 'a', 'a', 'a', 'a', 'a', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', '_', '_', '_'];
  var self			= this;

  for(var i = 0; i < chars.length; i++) {
    while(self.indexOf(String.fromCharCode(chars[i])) > -1) {
      self = self.replace(String.fromCharCode(chars[i]), charsReplace[i]);
    }
  }

  return self;
};


/**
 * Unformat Number
 * @returns {string}
 */
String.prototype.unFormatNumber = function() {
  var value = this;

  value = $.parseNumber(value, {format: "#,##0.0000", locale: 'pt'});
  if(isNaN(value)) {
    return "";
  }  else {
    return new String(value).toString();
  }
};


String.prototype.removeNotNumber = function() {
  let value = this;
  if (isNaN(value)) {
    value = value.replace(/([^0-9\.\,]){0,}/g, '').replace(/\./g, '').replace(/\,/g, '.');
    return isNaN(value) ? '' : value;
  }
  return this;
};


/**
 * Transform string to integer
 * @returns Number
 */
String.prototype.toInteger = function(){
  return parseInt(this.unFormatNumber().replace(/[^0-9.-]/g, "")) || 0;
};


/**
 * Transform string in a decimal valid value
 * @return Number
 */
String.prototype.toDecimal = function(){
  return parseFloat(this.unFormatNumber()) || 0;
};


/**
 * Transform first letter to upper case
 * @returns {string}
 */
String.prototype.toUpperFirstLetter = function(){
  return this.replace(/^[a-z]/, function (x) {return x.toUpperCase()});
};


String.prototype.isBlank = function () {
  return $.trim(this) === '';
};

String.prototype.toMoment = function(){
  const format = [
    'DD/MM/YYYY HH:mm'.slice(0, this.length),
    'DD/MM/YYYY'.slice(0, this.length),
    'YYYY-MM-DD HH:mm'.slice(0, this.length),
    'YYYY-MM-DD'.slice(0, this.length),
  ]
  const data = moment(this, format)

  return data.isValid() ? data : null;
};
