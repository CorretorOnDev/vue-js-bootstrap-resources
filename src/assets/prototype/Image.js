import $ from 'jquery'
Image.prototype.rotate = function (degree) {
  const rotate = {
    0: {
      canvasWidth: this.width,
      canvasHeight: this.height,
      width: this.width,
      height: this.height,
      x: 0,
      y: 0
    },
    90: {
      canvasWidth: this.height,
      canvasHeight: this.width,
      width: this.width,
      height: this.height,
      x: 0,
      y: -this.height
    },
    180: {
      canvasWidth: this.width,
      canvasHeight: this.height,
      width: this.width,
      height: this.height,
      x: -this.width,
      y: -this.height
    },
    270: {
      canvasWidth: this.height,
      canvasHeight: this.width,
      width: this.width,
      height: this.height,
      x: -this.width,
      y: 0
    }
  };
  const image = this;
  const rotateSelected = rotate[degree];
  const output = new Image();
  output.crossOrigin = "Anonymous";
  output.onload = function () {
    const canvas = document.createElement('canvas');
    const context = canvas.getContext('2d');
    canvas.width = rotateSelected.canvasWidth;
    canvas.height = rotateSelected.canvasHeight;
    context.imageSmoothingQuality = 'high';
    context.rotate(degree * Math.PI / 180);
    context.drawImage(image, rotateSelected.x, rotateSelected.y, rotateSelected.width, rotateSelected.height);
    canvas.toBlob(function (blob) {
      setTimeout(function () {
        $(image).triggerHandler('rotate', {blob: blob, dataURL: canvas.toDataURL()});
      }, 200)
    });
    image.src = canvas.toDataURL();
  };
  output.src = image.src;
};

Image.prototype.rotateNext = function () {
  this.rotate(90);
}
