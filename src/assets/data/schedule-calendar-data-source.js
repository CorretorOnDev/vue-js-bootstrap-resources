export const DAYS_WEEK = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab']
export const SCHEDULE_DAY = 'D'
export const SCHEDULE_WEEK = 'W'
export const SCHEDULE_MONTH = 'M'
export const SCHEDULE_COMMITMENT = 'C'

export const SCHEDULE_TYPE = {
  [SCHEDULE_DAY]: 'Dia',
  [SCHEDULE_WEEK]: 'Semana',
  [SCHEDULE_MONTH]: 'Mês',
  [SCHEDULE_COMMITMENT]: 'Compromissos'
}

export const initialValue = {
  title: null,
  allDay: false,
  startDate: null,
  endDate: null,
  color: null
};