import $ from 'jquery'

function setMask (el, binding, vnode) {
  $(el).unsetMask()
  switch (binding.value) {
    case 'phone':
      let phone = el.value.replace(/\D/g, '')
      if (phone.length > 10) {
        $(el).setMask({mask: '(99) 99999-9999', autoTab: false})
      } else {
        $(el).setMask({mask: '(99) 9999-99999', autoTab: false})
      }
      break
    default:
      if (binding.value) {
        $(el).setMask(binding.value)
      }
  }
  $(el).trigger('setMask', [$(el).val()])
}

export default {
  bind (el, binding, vnode) {
    setMask(el, binding, vnode)
  },
  update (el, binding, vnode) {
    setMask(el, binding, vnode)
  }
}
